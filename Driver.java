
public class Driver {

	public static void main(String[] args) {
		GeometricObject g = new Circle("YELLOW", true, 3.5);
		g.print();
		
		g = new Rectangle("PURPLE", false, 10, 8);
		g.print();
	
		
		g = new Triangle("PURPLE", false, 12, 100);
		g.print();
		
		Circle c1 = new Circle("YELLOW", true, 3.5);
		Circle c2 = new Circle("PUCE", false, 12);
		
		if(c1.compareTo(c2) < 0)
		{
			c1.print();
		}
		else if (c1.compareTo(c2) > 0)
		{
			c2.print();
		}
		else 
			System.out.println("Same size");
	}
}
