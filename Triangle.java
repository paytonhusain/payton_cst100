
public class Triangle extends GeometricObject {
	//EQ triangle
	private double base, height;
	
	public Triangle(String color, boolean fill, double base, double height) {
		super(color, fill);
		this.base = base;
		this.height = height;
	}

	@Override
	public double perimeter() {
		return base * 3;
	}

	@Override
	public double area() {
		return .5 * base * height;
		
	}
	@Override
	public int compareTo(GeometricObject arg0) {
		// make sure arg0 is a Rectangle
		if(arg0 instanceof Triangle){
			//it is - so compare the area of this and arg0
			if(this.area() < arg0.area())return -1;
			else if (this.area() > arg0.area())return 1;
			else return 0;
		}
		
		return -1000;
	}

	@Override
	public void howToColor() {
		// TODO Auto-generated method stub
		
	}
	
}
