//This is my own work
//Payton R Husain
//HW10, CST100, Fall 2016
package hw10;

public class MyDate {
	private int day,  month, year;

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(){
		this.month = 11;
		this.day= 29;
		this.year= 2016;
	}
	
	
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return month + "/" + day + "/" + year;
	}
	
	
	
	
}
