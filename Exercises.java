import java.util.Scanner;

public class Exercises {

	private static final int PASSWORD_REQUIRED_LENGTH = 0;


	public static void main(String[] args) {

		Scanner kb = new Scanner(System.in);
		int row, col;

		// char array of arrays
		char[][] board = new char[3][3];

		// ask player X for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an X and re-display it
		board[row][col] = 'X';
		showBoard(board);

		// ask player O for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an O and re-display it
		board[row][col] = 'O';
		showBoard(board);

		// ask player X for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an X and re-display it
		board[row][col] = 'X';
		showBoard(board);

		// ask player O for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an O and re-display it
		board[row][col] = 'O';
		showBoard(board);

		// ask player X for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an X and re-display it
		// CHECK FOR WIN - if so, say winner and display it
		board[row][col] = 'X';
		showBoard(board);
		if (win(board)) {
			System.out.println("X is the winner");
			System.exit(0);
		}
		// ask player O for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an O and re-display it
		// CHECK FOR WIN - if so, say winner and display it
		board[row][col] = 'O';
		showBoard(board);
		if (win(board)) {
			System.out.println("O is the winner");
			System.exit(0);
		}
		// ask player X for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an X and re-display it
		// CHECK FOR WIN - if so, say winner and display it
		board[row][col] = 'X';
		showBoard(board);
		if (win(board)) {
			System.out.println("X is the winner");
			System.exit(0);
		}
		// ask player O for row and col
		System.out.print("Enter row and col:  ");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an O and re-display it
		// CHECK FOR WIN - if so, say winner and display it
		board[row][col] = 'O';
		showBoard(board);
		if (win(board)) {
			System.out.println("O is the winner");
			System.exit(0);
		}
		// ask player X for row and col
		System.out.print("Enter row and col:");
		row = kb.nextInt();
		col = kb.nextInt();
		row--;
		col--;
		// mark the board with an X and re-display it
		// CHECK FOR WIN - if so, say winner and display it
		board[row][col] = 'X';
		showBoard(board);
		// else - say DRAW
		if (win(board)) {
			System.out.println("X is the winner");
		} else
			System.out.println("DRAW");
	}

	public static boolean win(char[][] b) {
		if (b[0][0] != ' ' && b[0][0] == b[0][1] && b[0][1] == b[0][2])
			return true;
		if (b[1][0] != ' ' && b[1][1] == b[1][1] && b[1][1] == b[1][2])
			return true;
		if (b[2][0] != ' ' && b[2][0] == b[2][1] && b[2][1] == b[2][2])
			return true;

		if (b[0][0] != ' ' && b[0][0] == b[1][0] && b[1][0] == b[2][0])
			return true;
		if (b[0][1] != ' ' && b[0][1] == b[1][2] && b[1][2] == b[2][1])
			return true;
		if (b[0][0] != ' ' && b[0][2] == b[1][2] && b[1][2] == b[2][2])
			return true;

		if (b[0][0] != ' ' && b[0][0] == b[1][1] && b[1][1] == b[2][2])
			return true;
		if (b[0][2] != ' ' && b[0][2] == b[1][1] && b[1][1] == b[2][0])
			return true;

		return false;
	}

	public static void showBoard(char[][] b) {
		System.out.println("-------");
		for (int r = 0; r < 3; r++) {
			for (int c = 0; c < 3; c++) {
				System.out.print("|" + b[r][c] + " ");

			}
			System.out.println("|");
			System.out.println("-------");

			Scanner input = new Scanner(System.in);
			System.out.print("Enter ten integers: ");
			int[] n = new int[10];

			for (int i = 0; i < 10; i++)
				n[i] = input.nextInt();

			for (int i = n.length - 1; i >= 0; i--)
				System.out.print(n[i] + " ");

			
			
			
			int randomArray[]=new int[101];
	        int countArray[]=new int[10];

	        
	        int j;
	        for(int i=1; i<101; ++i) {
	            j=i;
	                
					Scanner kb = null;
					randomArray[i]=kb.nextInt(10);

	            
	                  System.out.println("Number " + j + " = " + randomArray[i]);

	            
	                  
			
			
			
			
			
		}
	}
	}
	{

        Scanner input = new Scanner(System.in);
        System.out.print("Enter a integer: ");
        double num1 = input.nextDouble();
        double num2 = input.nextDouble();
        double num3 = input.nextDouble();

        displaySortedNumbers(num1,num2,num3);
    }

    public static void displaySortedNumbers( double num1, double num2, double num3) {

        double temp;
        if (num1 > num2) {
            temp = num1;
            num1 = num2;
            num2 = temp;
        }

        if (num2 > num3) {
            temp = num2;
            num2 = num3;
            num3 = temp;
        }
        if (num1 > num2) {
            temp = num1;
            num1 = num2;
            num2 = temp;
        }
        System.out.println(num1 + " " + num2 + " " + num3);
    }
    {

        String c = "Celsius";
        String f = "Fahrenheit";
        System.out.printf("%s%15s | %10s%10s\n", c, f, f, c);
        for (double celsius = 40, fahrenheit = 120.0; celsius >= 31; celsius--, fahrenheit -= 10) {

            System.out.printf("%4.2f%17.2f | %10.2f%10.2f\n", celsius, celsiusToFahrenheit(celsius),
                    fahrenheit, fahrenheitToCelsius(fahrenheit));

        }

    }

  
    public static double celsiusToFahrenheit(double celsius) {

        return (9.0 / 5.0) * celsius + 32.0;

    }

    /** Convert from Fahrenheit to Celsius */
    public static double fahrenheitToCelsius(double fahrenheit) {

        return (5.0 / 9) * (fahrenheit - 32.0);
    }
    {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter a integer: ");
        int num = input.nextInt();

        if (isPalindrome(num)) {
            System.out.println(num + " is a palindrome.");
        } else {
            System.out.println(num + " is NOT a palindrome.");

        }

    }

    public static int reverse(int number) {

        int reverse = 0;
        while (number != 0) {

            reverse *= 10; // is ignored first iteration
            reverse += number % 10;
            number /= 10;
        }

        return reverse;
    }

    public static boolean isPalindrome(int number) {

        return (number == reverse(number));
    }
    {

        Scanner input = new Scanner(System.in);
        System.out.print(
                " A password must have at least eight characters.\n" +
                " A password consists of only letters and digits.\n" +
                " A password must contain at least two digits \n" +
                "Enter a password meeting the requirements above: ");
        String s = input.nextLine();

        if (isValidPassword(s)) {
            System.out.println("Password is VALID: " + s);
        } else { 
            System.out.println("NOT VALID PASSWORD: " + s);
        }

    }

    public static boolean isValidPassword(String password) {

        if (password.length() < PASSWORD_REQUIRED_LENGTH) return false;

        int charCount = 0;
        int numCount = 0;
        for (int i = 0; i < password.length(); i++) {

            char ch = password.charAt(i);

            if (isNumeric(ch)) numCount++;
            else if (isLetter(ch)) charCount++;
            else return false;
        }


        return (charCount >= 2 && numCount >= 2);
    }

    public static boolean isLetter(char ch) {
        ch = Character.toUpperCase(ch);
        return (ch >= 'A' && ch <= 'Z');
    }


    public static boolean isNumeric(char ch) {

        return (ch >= '0' && ch <= '9');
    }


}


    
    



