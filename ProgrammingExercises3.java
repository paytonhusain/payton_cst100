import java.util.Random;
import java.util.Scanner;

//This is my own work. Payton Husain

public class ProgrammingExercises3 {

	public static void main(String [] args){
		
 Scanner input = new Scanner(System.in);
 
  			System.out.print("Enter a decimal value (0 to 15): ");

  			int i = input.nextInt();

  			if (i < 0 || i > 15) {
  				System.out.println(i + " is an invalid input");
  				System.exit(0);
  			}

  			System.out.println("The hex value is "
  					+ Integer.toHexString(i).toUpperCase());

		
 
 
 System.out.println("Enter Employee's name:");
        	String name = input.nextLine();
        	
        	System.out.println("Enter number of hours worked in a week:");
        	double hours = input.nextDouble();
        	
        	System.out.println("Enter hourly pay rate:");
        	double rate = input.nextDouble();
        	
        	System.out.println("Enter federal tax withholding rate:");
        	double fedtax = input.nextDouble();
        	
        	System.out.println("Enter state tax withholding rate:");
        	double statax = input.nextDouble();
        	
        	System.out.println("Employee Name:" + name);
        	System.out.println("Hours worked:" + hours);
        	System.out.println("Pay rate: $"+ rate);
        	System.out.println("Gross Pay: $" + rate * hours);
        	  System.out.println("Deduction:");
        	  System.out.printf("Federal Withholding (%.4f%%): $%.5f\n", fedtax * 100,
        	    fedtax * rate * hours);
        	  System.out.printf("State Withholding (%.4f%%): $%.5f\n", statax * 100,
        	    statax * rate * hours);
        	  System.out.printf("Total Deduction: $%.5f\n", (statax + fedtax) * rate
        	    * hours);
        	  System.out.printf("Net Pay: $%.5f\n", (1 - statax - fedtax) * rate * hours);
        	  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

	}
}
