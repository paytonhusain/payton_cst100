//This is my own work
//Payton R Husain
//HW10, CST100, Fall 2016
package hw10;

public class Employee extends Person {
	
	private double salary;
	private  MyDate hireDate;
	private String office;
	
	
	
	public Employee(String name, String address, String phone, String email, double salary, MyDate hireDate,
			String office) {
		super(name, address, phone, email);
		this.salary = salary;
		this.hireDate = hireDate;
		this.office = office;
	}
	
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public MyDate getHireDate() {
		return hireDate;
	}
	public void setHireDate(MyDate hireDate) {
		this.hireDate = hireDate;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	@Override
	public String toString() {
		return super.toString() +
				"\nEmployee [salary=" + salary + ", hireDate=" + hireDate + ", office=" + office + "]";
	}
	
	

}
