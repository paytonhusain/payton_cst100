
public class Rectangle extends GeometricObject {

	private double length, width;
	
	public Rectangle(double length, double width)
	
	public Rectangle(String color, boolean fill, double length, double width) {
		super(color, fill);
		this.length = length;
		this.width = width;
	}

	
	public double perimeter() {
		return 2*(length + width);
	}

	@Override
	public double area() {
		return length * width;
	}


	public double getLength() {
		return length;
	}


	public void setLength(double length) {
		this.length = length;
	}


	public double getWidth() {
		return width;
	}


	public void setWidth(double width) {
		this.width = width;
	}

	public void print(){
		String f = "";
		if(this.isFill())
			f= "solid";
			else 
				f= "outlined";		
		
		String c = this.getColor();
			
		System.out.println(c + " " + f + " rectangle.");
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Rectangle){
			//cast obj to a Rectangle
			Rectangle rhs = (Rectangle)obj;
			//see if the color is the same and the areas are equal
			if(rhs.getColor().equals(this.getColor()) &&
					rhs.area() == this.area())
				return true;
		}
		return false;
	}
	@Override
	public int compareTo(GeometricObject arg0) {
		// make sure arg0 is a Rectangle
		if(arg0 instanceof Rectangle){
			//it is - so compare the area of this and arg0
			if(this.area() < arg0.area())return -1;
			else if (this.area() > arg0.area())return 1;
			else return 0;
		}
		
		return -1000;
	}


	@Override
	public void howToColor() {
	System.out.println("Outline rectangle with " + this.getColor());
		
	}
	
}
