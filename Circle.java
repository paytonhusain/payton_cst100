
public class Circle extends GeometricObject {
	
	private double radius;
	
	public Circle(String color, boolean fill, double radius) {
		super(color, fill);
		this.radius = radius;
	}
	
	public Circle(double radius) {
		super();
		this.radius = radius;
		
	}
	
	/* public double getRadius() {
		 super();
		 this.radius = radius;
	 } */

	@Override
	public double perimeter() {
		return Math.PI * 2 * radius;
	}

	@Override
	public double area() {
		return Math.PI * radius * radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	public void print(){
		String f = "";
		if(this.isFill())
			f= "filled";
			else 
				f= "open";
		
		String c = this.getColor();
			
		System.out.println(c + " " + f + " circle.");
	}
	
	
	@Override
	public int compareTo(GeometricObject arg0) {
		// make sure arg0 is a Rectangle
		if(arg0 instanceof Circle){
			//it is - so compare the area of this and arg0
			if(this.area() < arg0.area())return -1;
			else if (this.area() > arg0.area())return 1;
			else return 0;
		}
		
		return -1000;
	}

	@Override
	public void howToColor() {
		System.out.println("Fill circle with " + this.getColor());
		
	}
	
}
