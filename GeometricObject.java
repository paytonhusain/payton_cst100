import java.util.Date;

public abstract class GeometricObject implements Comparable<GeometricObject>, Colorable {

	private String color;
	private boolean fill; 

	
	
	public GeometricObject(){
		color = "Burnt Sienna";
				fill = true;
	}
	
	//constructors
	public GeometricObject(String color, boolean fill) {
		this.color = color;
		this.fill = fill;
		
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public boolean isFill() {
		return fill;
	}


	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public void print(){
		String f = "";
		if(this.isFill())
			f= "solid";
			else 
				f= "open";
		
		String c = this.getColor();
			
		System.out.println(c + " " + f + " shape.");
	}
	
	public abstract double perimeter();
	public abstract double area();
	
	
}
