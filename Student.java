//This is my own work
//Payton R Husain
//HW10, CST100, Fall 2016
package hw10;

public class Student extends Person {

	 private final String classStatus;

	public Student(String name, String address, String phone, String email, String classStatus) {
		super(name, address, phone, email);
		this.classStatus = classStatus;
	}

	public String getClassStatus() {
		return classStatus;
	}

	@Override
	public String toString() {
		return super.toString() + "\nStudent [classStatus=" + classStatus + "]";
	} 
	 
	 public String getclassStatus() {
		 return classStatus;
	 }
	 
}
